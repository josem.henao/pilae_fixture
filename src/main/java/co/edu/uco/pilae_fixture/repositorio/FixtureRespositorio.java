package co.edu.uco.pilae_fixture.repositorio;

import co.edu.uco.pilae_fixture.entity.FixtureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface FixtureRespositorio extends JpaRepository<FixtureEntity, Serializable> {

}
