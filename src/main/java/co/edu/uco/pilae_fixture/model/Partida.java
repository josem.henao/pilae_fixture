package co.edu.uco.pilae_fixture.model;

import lombok.Data;

import java.util.Date;

@Data
public class Partida {
    private int id_partida;
    private int id_local;
    private int id_visitante;
    private Date fecha;
}
