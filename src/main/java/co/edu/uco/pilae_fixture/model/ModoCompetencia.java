package co.edu.uco.pilae_fixture.model;

import lombok.Data;

@Data
public class ModoCompetencia {
    private short idModoCompetencia;
    private String modo;
    private short numeroGrupos;
    private short numeroClasificados;
    private boolean partidoVuelta;

}
