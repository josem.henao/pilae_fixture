package co.edu.uco.pilae_fixture.model;


import lombok.Data;

@Data
public class Dia {
    private short idDia;
    private String dia;
}
