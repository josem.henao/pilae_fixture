package co.edu.uco.pilae_fixture.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class Fixture {
    private int idFixture;
    private int idTorneo;
//    @Size(min = 2, message = "Tamaño de la lista de equipos no válido, se necesitan al menos 2")
//    @NotEmpty(message = "Tamaño de la lista de equipos no puede ser vacio")
    private List equipos;
    private Date fechaInicio;
    private Date fechaFin;
    private List<Partida> partidas;
    private List<Short> diasJuego;
    private ModoCompetencia modoCompetencia;
}
