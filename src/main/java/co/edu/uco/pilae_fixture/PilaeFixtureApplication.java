package co.edu.uco.pilae_fixture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PilaeFixtureApplication {

    public static void main(String[] args) {
        SpringApplication.run(PilaeFixtureApplication.class, args);
    }

}
