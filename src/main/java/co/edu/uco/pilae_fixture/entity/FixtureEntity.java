package co.edu.uco.pilae_fixture.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "fixtures")
@Data
public class FixtureEntity {
    @Id
    @GeneratedValue
    @Column(name = "id_fixture", nullable = false, length = 15)
    private int idFixture;

    @Size(min = 1, max = 10, message = "Tamaño del idTorneo no válido")
    @NotEmpty(message = "idTorneo no puede ser vacío")
    @Column(name = "id_torneo", nullable = false, length = 10)
    private int idTorneo;

    @Size(min = 2, message = "Tamaño de la lista de equipos no válido, se necesitan al menos 2 equipos")
    @NotEmpty(message = "La lista de equipos no puede ser vacío")
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "fixture_equipo", joinColumns = @JoinColumn(name = "id_fixture"), inverseJoinColumns = @JoinColumn(name = "id_equipo"))
    private List<EquipoEntity> equipos;

    @Size(min = 1, max = 10, message = "Tamaño de fechaInicio no válido")
    @NotEmpty(message = "fechaInicio no puede ser vacía")
    @Column(name = "fecha_inicio", nullable = false)
    private Date fechaInicio;

    @Size(min = 1, max = 10, message = "Tamaño de fechaFin no válido")
    @NotEmpty(message = "fechaFin no puede ser vacía")
    @Column(name = "fecha_fin", nullable = false)
    private Date fechaFin;

    @OneToMany
    @JoinTable(name = "fixture_partida", joinColumns = @JoinColumn(name = "id_fixture"), inverseJoinColumns = @JoinColumn(name = "id_partida"))
    private List<PartidaEntity> partidas;

    @Size(min = 1, message = "Tamaño de la lista de dias de juego no válido, se necesita al menos 1 día específico")
    @NotEmpty(message = "La lista de dias de juego no puede ser vacío")
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "fixture_dia", joinColumns = @JoinColumn(name = "id_fixture"), inverseJoinColumns = @JoinColumn(name = "id_dia"))
    private List<DiaEntity> diasJuego;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "id_modo_competencia")
    private ModoCompetenciaEntity modoCompetencia;
}
