package co.edu.uco.pilae_fixture.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "partidas")
@Data
public class PartidaEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_partida", nullable = false, length = 20)
    private int idPartida;

    @Column(name = "id_torneo", nullable = false, length = 10)
    private int idLocal;

    @Column(name = "id_visitante", nullable = false, length = 10)
    private int idVisitante;

    @Column
    private Date fecha;
}
