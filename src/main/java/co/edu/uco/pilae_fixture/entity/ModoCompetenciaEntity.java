package co.edu.uco.pilae_fixture.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "modo_competencia")
@Data
public class ModoCompetenciaEntity {
    @Id
    @GeneratedValue
    @Column(name = "id_modo_competencia", nullable = false, length = 20)
    private short idModoCompetencia;

    @Column(name = "modo", nullable = false, length = 45)
    private String modo;

    @Column(name = "numero_grupos", length = 2)
    private short numeroGrupos;

    @Column(name = "numero_clasificados", length = 2)
    private short numeroClasificados;

    @Column(name = "partido_vuelta", length = 2)
    private boolean partidoVuelta;
}
