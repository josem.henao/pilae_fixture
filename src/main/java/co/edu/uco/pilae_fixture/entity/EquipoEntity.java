package co.edu.uco.pilae_fixture.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "equipos")
@Data
public class EquipoEntity {
    @Id
    @GeneratedValue
    @Column(name = "id_equipo", nullable = false, length = 20)
    private int idEquipo;
}
