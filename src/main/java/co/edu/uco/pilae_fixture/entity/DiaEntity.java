package co.edu.uco.pilae_fixture.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dias")
public class DiaEntity {
    @Id
    @Column(name = "id_dia", nullable = false, length = 2)
    private short idDia;

    @Column(name = "dia", nullable = false, length = 10)
    private String dia;
}
